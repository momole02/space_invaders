"""
    Copyright (C) 2020 Marc-Arnaud AYENON

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

"""

import pygame
import constants as cst

"""
Class for handling game huds, score and lives
"""


class HUD:
    def __init__(self):
        self.font = pygame.font.Font("fonts/QuirkyRobot.ttf" , 32)

    def handle(self, screen,  score, lives):
        score_surf = self.font.render("Score : {}".format(score) , True , cst.HUD_COLOR )
        lives_surf = self.font.render("Vies : {}".format(lives) , True , cst.HUD_COLOR )
        screen.blit(score_surf, (cst.WINDOW_WIDTH - 20 - score_surf.get_width(), 10) )
        screen.blit(lives_surf, (cst.WINDOW_WIDTH - 20 - lives_surf.get_width(), 54) )

"""
    Copyright (C) 2020 Marc-Arnaud AYENON

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

"""

import pygame
import constants as const
"""
Joueur 
"""
class Player :

    """
    Initialise les paramètre du joueur, la position initiale, la vitesse initiale
    """
    def __init__(self):

        self.dep_factor = 1.5 # facteur de deplacement
        self.speed = (0, 0)  # vitesse initiale
        # les differentes positions du joueur
        self.player_normal_img = pygame.image.load("art/player.png").convert_alpha() # l'image du joueur normal
        self.player_left_img = pygame.image.load("art/playerLeft.png").convert_alpha() # l'image du joueur qui part a gauche
        self.player_right_img = pygame.image.load("art/playerRight.png").convert_alpha() # l'image du joueur qui part a droite
        self.player_shield_img = pygame.image.load("art/shield.png").convert_alpha() # shield

        # gestion de la securité
        self.safe_mode = False  # mode de securité (le enemis ne peuvent plus lui faire du mal dans ce mode)
        self.safe_mode_delay = 2500 # durée du safe mode
        self.last_safe_mode_time = 0 # temps ou le joueur a été mis en safe mode pour la dernière fois

        # image actuelle
        self.current_player_img = self.player_normal_img

        #definir le rectangle du joueur
        self.rect = pygame.Rect(0  , 0 , 0 , 0)
        self.rect.size = self.current_player_img.get_size()
        self.position = (const.WINDOW_WIDTH/2 - self.rect.w/2, const.WINDOW_HEIGHT - 3*self.rect.height - 10 )

        # lives & score
        self.lives = 3  # player lives
        self.score = 0  # score

    """Met le joueur en securité"""
    def goInSafe(self):
        self.safe_mode = True
        self.last_safe_mode_time = pygame.time.get_ticks()

    """Remet le joueur en position initiale"""
    def initialPosition(self):
        self.position = (const.WINDOW_WIDTH / 2 - self.rect.w / 2, const.WINDOW_HEIGHT - 3*self.rect.height - 10)
        self.updateRect()

    """
    Met a jour le rectangle du joueur
    """
    def updateRect(self):
        self.rect.topleft = self.position
        self.rect.size = self.current_player_img.get_size()



    """
    deplace le joueur en fonction de la vitesse
    """
    def move(self):
        x, y = self.position
        speed_x , speed_y = self.speed
        # appliquer la vitesse
        x += speed_x
        y += speed_y
        if 0 <= x <= const.WINDOW_WIDTH - 1 and 0 <= y <= const.WINDOW_HEIGHT - 1:
            self.position = (x , y)
            self.updateRect()


    """
    modifie la vitesse en fonction de l'état des touches de claver
    """
    def drive(self):
        keys = pygame.key.get_pressed()
        # determiner le vecteur vitesse en fonction de l'état des touches
        speed_y = self.dep_factor * (keys[pygame.K_DOWN] - keys[pygame.K_UP])
        speed_x = self.dep_factor * (keys[pygame.K_RIGHT] - keys[pygame.K_LEFT])

        if keys[pygame.K_LEFT]: # virer vers la gauche
            self.current_player_img = self.player_left_img

        elif keys[pygame.K_RIGHT]: # virer vers la droite
            self.current_player_img = self.player_right_img

        else: # garder l'aspect normal
            self.current_player_img = self.player_normal_img

        self.rect.size = self.current_player_img.get_size()
        self.speed = (speed_x, speed_y)

    def display(self, screen):

        if self.safe_mode: # afficher le bouclier lorsqu'il est en securité
            sw , sh = self.player_shield_img.get_width() , self.player_shield_img.get_height()
            player_x , player_y = self.position
            shield_x = player_x + self.rect.w/2 - sw/2
            shield_y = player_y + self.rect.h/2 - sh/2
            screen.blit(self.player_shield_img ,  (shield_x ,shield_y ))

            # enlever le joueur en safe mode si le temps arrive
            actual_time = pygame.time.get_ticks()
            if actual_time - self.last_safe_mode_time >= self.safe_mode_delay:
                self.safe_mode = False
                self.last_safe_mode_time = 0

        #afficher l'image actuelle
        screen.blit(self.current_player_img , self.position)


"""
    Copyright (C) 2020 Marc-Arnaud AYENON

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

"""

hiscore = 0

def set_hiscore(score):
    with open("score" , "w") as f:
        f.write(f"{score}")

def get_hiscore():
    try:
        with open("score", "r") as f:
            score = f.read()
        return int(score)
    except Exception as e:
        print(e)
        return 0
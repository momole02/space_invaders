"""
    Copyright (C) 2020 Marc-Arnaud AYENON

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

"""

import pygame
import random

# chargement des images un fois pour toute
# le fait de charger des images dans les __init__ prend beaucoup de memoire
red_laser_img = pygame.image.load("art/laserRed.png") # laser rouge
red_laser_explode_img = pygame.image.load("art/laserRedShot.png") # explosion laser rouge
green_laser_img = pygame.image.load("art/laserGreen.png") # laser vert
green_laser_explode_img = pygame.image.load("art/laserGreenShot.png") # explosion laser vert


EXPLODE_RED = 1
EXPLODE_GREEN = 0
"""
Classe permettant de gérer les balles et leurs mouvements
"""
class Bullet:

    """
    Initialise la balle avec sa position et sa vitesse
    """
    def __init__(self, position):
        self.position = position
        self.speed = (0, -2) # la balle vas toujours vers le haut
        self.rect = pygame.Rect(0,0,0,0)

        # choisir aleatoirement une image
        imgs = [red_laser_img , green_laser_img]
        r=random.randint(0,1)
        self.type = r
        self.image = imgs[r].convert_alpha()
        self.updateRect()

    """
    Mettre a jour le rectangle
    """
    def updateRect(self):
        self.rect.topleft = self.position
        self.rect.size = self.image.get_size()

    """
    Deplace la balle 
    """
    def move(self):
        x, y = self.position
        sx , sy = self.speed
        x += sx
        y += sy
        self.position = (x,y)
        self.updateRect()

    def display(self,screen):
        screen.blit(self.image , self.position)

"""
Generateur de balles multiples
"""
class BulletGenerator:
    """
    Paramétrage initial du générateur de balles
    """
    def __init__(self):
        self.bullets = [] # liste des balles


    """
    Generer une nouvelle balle en fonction de la position initiale
    """
    def generateBullet(self, position):
        self.bullets += [Bullet(position)]


    """
    Affiche/deplace toutes les balles
    """
    def handleBullets(self,screen):
        for bullet in self.bullets:
            bullet.display(screen)
            bullet.move()
            _, y = bullet.rect.bottomleft
            if y < 0 : # retirer les balles qui sortent de l'ecran pour ne pas gaspiller la mémoire
                self.bullets.remove(bullet)


"""
Classe permettant de materialiser une explosion
"""
class Explosion:
    def __init__(self , position, explode_type):
        self.explode_delay = 200 # duree de l'explosion
        if explode_type == EXPLODE_GREEN:
            self.image = green_laser_explode_img.convert_alpha()
        elif explode_type == EXPLODE_RED:
            self.image = red_laser_explode_img.convert_alpha()
        self.generation_time = pygame.time.get_ticks()
        self.position = position

    def handle(self ,screen):
        screen.blit(self.image, self.position)


"""
  
Particule d'explosion
"""
class ExplosionParticles:

    def __init__(self):
        self.explosions = []

    def explode(self, position, type):
        self.explosions.append(Explosion(position, type))

    def handle(self, screen):
        for exp in self.explosions:
            exp.handle(screen)
            actual_time = pygame.time.get_ticks()
            if actual_time - exp.generation_time >= exp.explode_delay:
                self.explosions.remove(exp)


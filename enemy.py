"""
    Copyright (C) 2020 Marc-Arnaud AYENON

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

"""

import pygame
import math, random
import constants as const

enemy_img = pygame.image.load("art/enemyShip.png")
enemy_ufo_img = pygame.image.load("art/enemyUFO.png")

"""
Classe permettant de gerer un ennemi
"""
class Enemy:

    """
    Paramètre un nouvel enemy
    :param position - la position de l'ennemi
    :param attack_angle - angle d'attaque de l'ennemi entre 0 et 180 degres
    :param attack_delay - durée avant l'attaque
    """

    def __init__(self , position , attack_angle , attack_delay , attack_speed):
        self.position = position
        self.attack_angle = attack_angle
        self.attack_delay = attack_delay
        self.attack_speed = attack_speed
        self.creation_time = pygame.time.get_ticks()
        self.rect = pygame.Rect(0,0,0,0)
        r=random.randint(0 , 30)
        if r > 26: # 4/30 d'avoir un OVNI
            self.image = enemy_ufo_img.convert_alpha()
        else:
            self.image = enemy_img.convert_alpha()


    """
    Mettre a jour le rectangle
    """
    def updateRect(self):
        self.rect.topleft = self.position
        self.rect.size = self.image.get_size()

    """
    Déplace l'ennemi pour attaquer
    """
    def attack(self):
        # calculer le vecteur vitesse en fonction de l'angle et de la vitesse
        # on va faire un peu de trigo ;)
        attack_angle_rad = self.attack_angle * math.pi / 180  # convertir l'angle d'attaque en radians
        speed_x = self.attack_speed * math.cos(attack_angle_rad)
        speed_y = self.attack_speed * math.sin(attack_angle_rad)

        # appliquer le deplacement
        x,y = self.position
        x += speed_x
        y += speed_y
        self.position = (x,y)

        # mettre a jour le rectangle
        self.updateRect()

    """
    Gerer l'ennemi afficher/attaquer etc..
    """
    def handle(self,screen):
        screen.blit(self.image, self.position)
        # quand attaquer ?
        actual_time = pygame.time.get_ticks()
        if actual_time - self.creation_time >= self.attack_delay : # a l'attaque !!!
            self.attack()



"""
Usine a ennemis
"""
class EnemyFactory:

    """""
    Inititalise le generateur des ennemis
    """
    def __init__(self):
        self.enemies = []
        # LE COMPORTEMENT DES ENEMIS SE GERE ICI #
        self.generation_delay = 1000 # duree en ms avant la generation du prochain ennemi
        self.min_attack_delay = 200 # duree minimale avant qu'un ennemi attaque
        self.max_attack_delay = 500 # duree maximale avant qu'un ennemi attaque
        self.last_generation_time = pygame.time.get_ticks() # temps de la dernière generation d'ennemis
        self.min_enemies_per_gen = 1  # nombre minimaæ d'ennemis à génerer d'un coup
        self.max_enemies_per_gen = 3 # nombre maximal d'ennemis à génerer d'un coup


    def generate(self):
        n_enemies = random.randint(self.min_enemies_per_gen , self.max_enemies_per_gen)
        for k in range(n_enemies):
            enemy_x = random.randint(10 , const.WINDOW_WIDTH - 100)

            if enemy_x > const.WINDOW_WIDTH / 2:
                attack_angle = random.randint(90, 120)
            else:
                attack_angle = random.randint(30, 90)

            dice = random.randint(0, 30)
            if dice <= 25 : #25/30 de generer des ennemis en haut
                enemy_y = -100
            else: #5/30 de generer des ennemis en bas
                enemy_y = const.WINDOW_HEIGHT
                attack_angle = -attack_angle #inverser l'angle d'attaque

            speeds = [0.3 , 0.5 , 0.7 ]
            attack_speed = speeds[random.randint(0 , len(speeds)-1)]
            attack_delay = random.randint(self.min_attack_delay , self.max_attack_delay)
            self.enemies += [Enemy((enemy_x, enemy_y) , attack_angle  , attack_delay , attack_speed )]

    def handleEnemies(self,screen):
        for enemy in self.enemies:
            enemy.handle(screen)
            _ , y = enemy.position
            if y > const.WINDOW_HEIGHT: # des qu'un enemi sort on le retire
                self.enemies.remove(enemy)

        # generer les enemis
        actual_time = pygame.time.get_ticks()
        if actual_time - self.last_generation_time >= self.generation_delay:
            self.generate()
            self.last_generation_time = pygame.time.get_ticks()
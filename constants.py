"""
    Copyright (C) 2020 Marc-Arnaud AYENON

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

"""

##########################################
# constants.py
##########################################
# definit les constantes du jeu
##########################################


# Dimensions de la fenêtre
WINDOW_WIDTH = 850
WINDOW_HEIGHT = 800

# Couleur des HUDs
HUD_COLOR = (255, 255, 255)

# la couleur de fond
BACKGROUND = (94, 63, 107)

# couleur du logo
LOGO_COLOR = (162, 179, 32)

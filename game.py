"""
    Copyright (C) 2020 Marc-Arnaud AYENON

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

"""

import pygame

from player import *
from bullet import *
from enemy import *
import constants as cst
import physics
from scene import *
from hud import *
import state, score

"""
Gère le gameplay 
"""
class Game:
    """
    Initialise tout les éléments du jeu
    décors, HUD, joueur, enemis, etc...
    """
    def __init__(self):
        self.hud = HUD()

        # le joueur
        self.player = Player()
        self.player.goInSafe()  # mettre le joueur en safe mode pour le debut du jeu

        # le lanceur de balles
        self.bullet_generator = BulletGenerator()

        # construire tous les ennemis
        self.enemies_factory = EnemyFactory()

        # les particules d'explosion
        self.explosion_particles = ExplosionParticles()

        # le decor
        self.scene = Scene()

        self.paused = False # jeu en pause ?
        self.showScene = True # afficher le decor ?

    """
    Reset game 
    """
    def resetGame(self):
        self.player = Player()
        self.player.goInSafe()  # mettre le joueur en safe mode pour le debut du jeu
        self.bullet_generator = BulletGenerator()
        self.enemies_factory = EnemyFactory()
        self.explosion_particles = ExplosionParticles()
        self.scene = Scene()

    """
    Gère les evenements du jeu 
    """
    def handleEvent(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:  # tire une balle
                self.bullet_generator.generateBullet(self.player.rect.center)

            if event.key == pygame.K_ESCAPE:
                self.paused = not self.paused

            if event.key == pygame.K_F2:
                self.showScene = not self.showScene


    """
    Gère l'affichage et les autre trucs 
    """
    def handle(self,screen):
        screen.fill(cst.BACKGROUND)  # remplir avec la couleur de fond

        if not self.paused:  # S'il n'y a pas de pause
            # Gerer le decor
            if self.showScene:
                self.scene.handle(screen)

            # Gérer les balles
            self.bullet_generator.handleBullets(screen)

            # Gérer les ennemis
            self.enemies_factory.handleEnemies(screen)

            # Gérer le joueur
            self.player.drive()  # conduire le joueur (ajuster la vitesse en fonction des touches)
            self.player.move()  # deplacer le joueur en fonction de la vitesse
            self.player.display(screen)  # afficher le joueur

            self.explosion_particles.handle(screen)

            physics.handleBulletsCollisions(self.player, self.bullet_generator, self.enemies_factory, self.explosion_particles)
            playerState = physics.handlePlayerCollisions(self.player, self.enemies_factory, self.explosion_particles)
            if playerState == 'KILLED':
                # gerer le hiscore
                score.hiscore = score.get_hiscore()
                if self.player.score > score.hiscore:
                    score.set_hiscore(self.player.score)
                    score.hiscore = self.player.score
                state.current_state = state.GS_MENU
                self.resetGame()
        else:
            surf = self.hud.font.render("Paused !", True, cst.HUD_COLOR)
            screen.blit(surf, ((cst.WINDOW_WIDTH - surf.get_width()) / 2, (cst.WINDOW_HEIGHT - surf.get_height()) / 2))

        self.hud.handle(screen, self.player.score, self.player.lives)


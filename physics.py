"""
    Copyright (C) 2020 Marc-Arnaud AYENON

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

"""

#############################################
# physics.py
#############################################
# fonctions et classes permettant de
# gerer toute la physique du jeu
#############################################

import state

"""Gerer les collisions entre les balles et les enemis"""

def handleBulletsCollisions(player, bullet_generator , enemies_factory, explosion_particles):
    for bullet in bullet_generator.bullets :
        for enemy in enemies_factory.enemies :
            if bullet.rect.colliderect(enemy.rect): # une balle touche un ennemi
                try:
                    enemies_factory.enemies.remove(enemy)
                    bullet_generator.bullets.remove(bullet)
                except ValueError as e: # eviter quelques bugs
                    print(e)

                explosion_particles.explode(enemy.rect.center , bullet.type)
                # augmenter le score
                player.score += 10

"""Gerer les collision entre les enemis et le joueur"""
def handlePlayerCollisions(player , enemies_factory, explosion_particles):
    for enemy in enemies_factory.enemies:
        if enemy.rect.colliderect(player.rect): # si un enemi touche le joueur
            enemies_factory.enemies.remove(enemy) # tuer l'ennemi
            explosion_particles.explode(enemy.rect.center, 0)
            player.score += 10
            if not player.safe_mode : # le joueur n'est pas dans la position de securité
                # retirer les vies du joueur
                player.lives -= 1
                if player.lives == 0: # plus de vies
                    return 'KILLED'
#                    player.initialPosition() # remet le joueur a sa position initiale
                player.goInSafe()  # passe en safe mode
    return 'ALIVE'
"""
    Copyright (C) 2020 Marc-Arnaud AYENON

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

"""

from game import *
from menu import *
import state
if __name__ == "__main__":
    pygame.init()
    screen = pygame.display.set_mode((const.WINDOW_WIDTH, const.WINDOW_HEIGHT))
    pygame.display.set_caption("Space invaders")

    state.current_state = state.GS_MENU

    menu = Menu()
    game = Game()
    running = True
    while running :
        for event in pygame.event.get() :
            if event.type == pygame.QUIT :
                running = False
            if state.current_state == state.GS_MENU:
                menu.handleEvent(event)
            elif state.current_state == state.GS_GAME:
                game.handleEvent(event)

        if state.current_state == state.GS_MENU:
            menu.handle(screen)
        elif state.current_state == state.GS_GAME:
            game.handle(screen)

        pygame.display.flip()





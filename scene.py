"""
    Copyright (C) 2020 Marc-Arnaud AYENON

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

"""

#########################################
# scene.py
#########################################
# gestion du decor
#########################################
import pygame
import constants as cst
import random
import copy

nebula_img = pygame.image.load("art/Background/nebula.png")
big_star_img= pygame.image.load("art/Background/starBig.png")
small_star_img = pygame.image.load("art/Background/starSmall.png")
big_meteor_img = pygame.image.load("art/meteorBig.png")
small_meteor_img = pygame.image.load("art/meteorSmall.png")

"""Represente un objet de la scene(nuage, etoile, petite, etoile, asteroide ... )"""


class SceneObject:

    def __init__(self, image , position , speed):
        self.image = image.convert_alpha()
        self.position = position
        self.speed = speed

    def move(self):
        x , y = self.position
        sx , sy = self.speed
        x += sx
        y += sy
        self.position = (x,y)

    def handle(self, screen):
        screen.blit(self.image, self.position)
        self.move()


class Scene:

    def __init__(self):
        self.objects = []
        self.next_gen_time = pygame.time.get_ticks() + 300
        # objets
        self.nebula = SceneObject(nebula_img , (0,0) , (0 , 0.3) )
        self.small_star = SceneObject(small_star_img , (0,0) , (0 , 0.3) )
        self.big_star = SceneObject(big_star_img , (0,0) , (0 , 0.4) )
        self.small_meteor = SceneObject(small_meteor_img , (0,0) , (0 , 0.5) )
        self.big_meteor = SceneObject(big_meteor_img , (0,0) , (0 , 0.2) )

    def generate(self):
        x = random.randint(0 , cst.WINDOW_WIDTH-1)
        y = -110
        rnd = random.randint(0 , 100)
        if 0 <= rnd <= 5 : #generer un nuage
            object = copy.copy(self.nebula) # copier le nuage
        elif 5 <= rnd <= 40 : # generer une petite etoile
            object = copy.copy(self.small_star)
        elif 40 <= rnd <= 55 : # generer une grande etoile
            object = copy.copy(self.big_star)
        elif 55 <= rnd <= 60 : # generer un petit asteroide
            object = copy.copy(self.small_meteor)
        elif 60 <= rnd <= 63 : # generer un grand asteroid
            object = copy.copy(self.big_meteor)
        else :
            object = None
        if object is not None:
            object.position = (x, y)
            self.objects += [object]

    def handle(self,screen):
        # Gerer les objets du decor
        for obj in self.objects :
            obj.handle(screen)
            px , py = obj.position
            if px >= cst.WINDOW_WIDTH or py >= cst.WINDOW_HEIGHT:
                self.objects.remove(obj)

        # Generer les elements du decor
        actual_time = pygame.time.get_ticks()
        if actual_time > self.next_gen_time:
            self.generate()
            delay = random.randint(100, 500)
            self.next_gen_time = pygame.time.get_ticks() + delay

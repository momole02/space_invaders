"""
    Copyright (C) 2020 Marc-Arnaud AYENON

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

"""

import pygame,math
import state, score
import constants as cst

"""
Classe permettant de gérer le menu
"""
class Menu:

    def __init__(self):
        self.fontBig = pygame.font.Font("fonts/QuirkyRobot.ttf", 128)
        self.fontNormal = pygame.font.Font("fonts/QuirkyRobot.ttf", 64)
        self.fontSmall = pygame.font.Font("fonts/QuirkyRobot.ttf", 32)
        self.logo_anim_frame = 0
        score.hiscore = score.get_hiscore()



    def handleEvent(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            state.current_state = state.GS_GAME

    def handle(self,screen):
        screen.fill(cst.BACKGROUND)
        f = self.logo_anim_frame
        x_anim = 10 * math.cos(f * math.pi / 180)
        y_anim = 10 * math.sin(f * math.pi / 180)
        logo_surf = self.fontBig.render("Space Invaders", True, cst.LOGO_COLOR)
        screen.blit(logo_surf,
                    ((cst.WINDOW_WIDTH - logo_surf.get_width()) / 2 + x_anim, cst.WINDOW_HEIGHT / 5 + y_anim))

        play_surf = self.fontNormal.render("Cliquez pour commencer !!", True, cst.HUD_COLOR)
        screen.blit(play_surf, ((cst.WINDOW_WIDTH - play_surf.get_width()) / 2, 2 * cst.WINDOW_HEIGHT / 5))

        hiscore_surf = self.fontSmall.render("Hi-Score : {}".format(score.hiscore), True, cst.HUD_COLOR)
        screen.blit(hiscore_surf, (cst.WINDOW_WIDTH - hiscore_surf.get_width() - 20, 10))

        by_surf = self.fontSmall.render("By momole02", True, cst.HUD_COLOR)
        screen.blit(by_surf, (0, cst.WINDOW_HEIGHT - 32))

        self.logo_anim_frame = (self.logo_anim_frame + 1) % 360

